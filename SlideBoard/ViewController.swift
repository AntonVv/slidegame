//
//  ViewController.swift
//  SlideBoard
//
//  Created by Mac on 5/9/19.
//  Copyright © 2019 AV. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet private weak var resultLabel: UILabel!
    
    private var game: GameSession?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        do {
            self.game = try GameSession()
            self.game?.gameSolution.delegate = self
            
            try self.game?.gameSolution.solve()
            
        } catch {
            print(error)
        }
    }

    
    fileprivate func printResultWith(delay: Double = 0.02, snapshots: [String]) {
        var i = 0

        DispatchQueue.main.async {
            
            Timer.scheduledTimer(withTimeInterval: delay, repeats: true) { [weak self] timer in
                
                guard let `self` = self else {
                    return
                }
                
                if i < snapshots.count {
                    self.resultLabel.text = snapshots[i]
                    i += 1
                } else {
                    timer.invalidate()
                }
            }
        }
    }
}

extension ViewController: GameSessionDelegate {
    
    func didFind(solution: [String]) {
        let best = solution
        self.printResultWith(snapshots: best)
    }
}
