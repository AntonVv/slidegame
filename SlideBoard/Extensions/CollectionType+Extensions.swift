//
//  CollectionType+Extensions.swift
//  SlideBoard
//
//  Created by Mac on 5/24/19.
//  Copyright © 2019 AV. All rights reserved.
//

import Foundation

extension Collection {
    
    func forEach(body: ((Self.Iterator.Element, Int, inout Bool) -> Void)) {
        var stop = false
        let enumerate = self.enumerated()
        for (index,value) in enumerate {
            if stop { break }
            body(value,index,&stop)
        }
    }
}

extension LazySequence {
    func forEach(body: ((LazySequence.Iterator.Element, Int, inout Bool) -> Void)) {
        var stop = false
        let enumerate = self.enumerated()
        for (index,value) in enumerate {
            if stop { break }
            body(value,index,&stop)
        }
    }
}
