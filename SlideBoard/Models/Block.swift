//
//  Block.swift
//  SlideBoard
//
//  Created by Mac on 5/10/19.
//  Copyright © 2019 AV. All rights reserved.
//

import Foundation

class Block {
    let size: Size
    let id: String
    var position: Position

    init(size: Size, at position: Position, id: String) {
        self.size = size
        self.position = position
        self.id = id
    }
    
    convenience init(size: Size, at position: Position) {
        self.init(size: size, at: position, id:"?")
    }
}

extension Block: Hashable {
    
    static func == (lhs: Block, rhs: Block) -> Bool {
        return lhs.id == rhs.id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

extension Block: CustomDebugStringConvertible {
    var debugDescription: String {
        return "\(self.id) at \(self.position) with \(self.size)"
    }
}


extension Block {
    
    var absolutePoints: [Position] { //array of Points that filled by Block on desk
        
        var points = [Position]()
        for x in 0..<self.size.width {
            
            let xCoordinate = self.position.x + x
            
            for y in 0..<self.size.height {
                let yCoordinate = self.position.y + y
                
                points.append(Position(xCoordinate,
                                       yCoordinate))
            }
        }
        
        return points
    }
    
    static var borderBlock: Block {
        let block = Block(size: Size(1, 1), at: Position.zero, id: BlockIdentifierGenerator.borderIdentifier)
        return block
    }
    
    static var gateBlock: Block {
        let block = Block(size: Size(1, 1), at: Position.zero, id: BlockIdentifierGenerator.gateIdentifier)
        return block
    }
}


final class BlockIdentifierGenerator {
    static let shared = BlockIdentifierGenerator()

    static let borderIdentifier = "X"
    static let gateIdentifier = "Z"
    
    private var charString = "abcdefghijklmnopqrstuvwxyz"
    

    func getNewIdentifier() -> String {
        if !charString.isEmpty {
            let index = Int.random(in: 0 ..< charString.count)
            return String(charString.remove(at: charString.index(charString.startIndex, offsetBy: index)))
        }
        return "?"
    }
}

