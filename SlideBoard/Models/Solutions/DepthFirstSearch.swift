//
//  DepthFirstSearch.swift
//  SlideBoard
//
//  Created by Mac on 5/30/19.
//  Copyright © 2019 AV. All rights reserved.
//

import Foundation


final class DepthFirstSearchSolution: GameSolution {
    
    weak var delegate: GameSessionDelegate?
    private weak var initialBoard: Board?
    
    init(board: Board) {
        self.initialBoard = board
    }
    
    func solve() throws {
        
        guard let initialBoard = self.initialBoard else {
            throw BoardError.isNotAvailable
        }
        var resultBoards = [Int:[Board]]()
        
        var queue = [initialBoard]
        var visitedBoards: Set<Board> = []
        
        
        // remove nodes at the same level form the queue (can be used for searching best solution from finded node)
        func removeBoardsFromTheSameLevelAs(board: Board) {
            var numToClean = 0
            queue.removeAll { (boardToRemove) -> Bool in
                let remove = boardToRemove.moveNumber >= board.moveNumber
                if remove {
                    numToClean += 1
                    visitedBoards.remove(boardToRemove)
                }
                return remove
            }
            print("REMOVED \(numToClean) items on the same deep")
        }
        
        func printSolution() {
            if let bestKey = resultBoards.keys.sorted().first, let bestBoard = resultBoards[bestKey] {
                print("BEST Result in \(bestKey) steps")
                self.delegate?.didFind(solution: bestBoard.map{$0.debugDescription})
            }
        }
        
        var bestResultDeep = Int.max
        
        while !queue.isEmpty {
            
            guard let board = queue.popLast() else {
                throw GameError.invalidResult
            }
            visitedBoards.insert(board)
            
            if board.isMainBlockExited() {
                let boards = board.boardChain()
                print("new Result in \(boards.count) steps")
                removeBoardsFromTheSameLevelAs(board: board)
                if board.moveNumber < bestResultDeep {
                    bestResultDeep = board.moveNumber
                }
                
                resultBoards[boards.count] = boards
                break
            }
            
            let newMoves = board.getAllAvailableMoves()
            for move in newMoves {
                let nextBoard = try self.apply(move: move, to: board)
                
                if nextBoard.moveNumber >= bestResultDeep {
                    continue
                }
                
                if !visitedBoards.contains(nextBoard) {
                    queue.append(nextBoard)
                } else {
                    
                }
            }
        }
        print("End of attemps")
        printSolution()
    }
    
}
