//
//  PrioritizeSolution.swift
//  SlideBoard
//
//  Created by Mac on 5/30/19.
//  Copyright © 2019 AV. All rights reserved.
//

import Foundation

final class PrioritizeSolution: GameSolution {
    var delegate: GameSessionDelegate?
    private weak var initialBoard: Board?

    init(board: Board) {
        self.initialBoard = board
    }
    
    
    func prioritizeMovesForMainOrNeigbourhoodOn(board: Board) -> [Move] {
        
        guard let mainBlockPosition = board.blocks["b"]?.position else {
            return []
        }
        
        var prioritizedMoves = [Move]()
        for move in board.getAllAvailableMoves() {
            
            var newMove = move

            if move.block.id != "b" && move.direction == .top {
                newMove.priority += 1
            }
            let newBoard = try? self.apply(move: move, to: board)
            if let newMainBlockPosition = newBoard?.blocks["b"]?.position {
                if newMainBlockPosition.y > mainBlockPosition.y {
                    newMove.priority += 1
                }
            }
            prioritizedMoves.append(newMove)
        }
        
        prioritizedMoves.sort { lft, rght -> Bool in
            return lft.priority < rght.priority
        }
        return prioritizedMoves
    }
    
    func solve() throws {
        
        guard let initialBoard = self.initialBoard else {
            throw BoardError.isNotAvailable
        }
        let find = 1
        var results = 0
        var resultBoards = [Int:[Board]]()
        
        
        var queue = [initialBoard]
        var visitedBoards: Set<Board> = []
        
        func removeBoardsFromTheSameLevelAs(board: Board) {
            var numToClean = 0
            queue.removeAll { (boardToRemove) -> Bool in
                let remove = boardToRemove.moveNumber >= board.moveNumber
                if remove {
                    numToClean += 1
                    visitedBoards.remove(boardToRemove)
                }
                return remove
            }
            print("REMOVED \(numToClean) items on the same deep")
        }
        
        func printSolution() {
            if let bestKey = resultBoards.keys.sorted().first, let bestBoard = resultBoards[bestKey] {
                print("BEST Result in \(bestKey) steps")
                self.delegate?.didFind(solution: bestBoard.map{$0.debugDescription})
            }
        }
        
        
        var bestResultDeep = Int.max
        
        while !queue.isEmpty {
            
            guard let board = queue.popLast() else {
                throw GameError.invalidResult
            }
            visitedBoards.insert(board)
            
            if board.isMainBlockExited() {
                let boards = board.boardChain()
                print("new Result in \(boards.count) steps")
                removeBoardsFromTheSameLevelAs(board: board)
                if board.moveNumber < bestResultDeep {
                    bestResultDeep = board.moveNumber
                }
                
                resultBoards[boards.count] = boards
                results += 1
                
                if results >= find {
                    break
                }
                continue
            }
            
            let newMoves = self.prioritizeMovesForMainOrNeigbourhoodOn(board: board)
            for move in newMoves {
                let nextBoard = try self.apply(move: move, to: board)
                
                if nextBoard.moveNumber >= bestResultDeep {
                    continue
                }
                
                if !visitedBoards.contains(nextBoard) {
                    queue.append(nextBoard)
                } else {
                    
                }
            }
        }
        print("End of attemps")
        printSolution()
    }
}

