//
//  GameSession.swift
//  SlideBoard
//
//  Created by Mac on 5/12/19.
//  Copyright © 2019 AV. All rights reserved.
//

import Foundation


/*
 X - border
 Z - gate for main block
 b - main block shoud exist
 
 General assumptions:
 - board is rectangular form
 - all blocks are rectangular form
 - all blocks has their own unique name
*/
class BoardInputJSON {
    
    static var initialPositionSnapshot = [["X","X","X","X","X","X"],
                                          ["X","a","b","b","c","X"],
                                          ["X","a","b","b","c","X"],
                                          ["X","d","e","e","f","X"],
                                          ["X","d","g","h","f","X"],
                                          ["X","i"," "," ","j","X"],
                                          ["X","X","Z","Z","X","X"]]
    
    
}

protocol GameSessionDelegate: class {
    func didFind(solution: [String])
}

class GameSession {
    var board: Board
    var gameSolution: GameSolution

    init(board: Board, gameSolution: GameSolution) {
        self.board = board
        self.gameSolution = gameSolution
    }
    
    convenience init() throws {
        let board = try Board(snapshot: BoardInputJSON.initialPositionSnapshot)
        let gameSolution = DepthFirstSearchSolution(board: board)
        self.init(board: board, gameSolution:gameSolution)
    }
}




protocol GameSolution {
    var delegate: GameSessionDelegate? { get set}
    init(board: Board)
    func solve() throws
}



extension GameSolution {
    
    
    func apply(move: Move, to board: Board) throws -> Board {
        
        board.move = move
        
        let blockName = move.block.id
        let newPosition = move.getPredictedPositions()
        
        var newBoardSnapshot = [[String]]()
        for y in 0 ..< board.size.height {
            
            var new_y = [String]()
            for x in 0 ..< board.size.width {
                let cellSnapshot = board.getBlock(atX: x, y: y)?.id ?? " "
                
                if cellSnapshot == BlockIdentifierGenerator.gateIdentifier || cellSnapshot == BlockIdentifierGenerator.borderIdentifier {
                    new_y.append(cellSnapshot)
                    
                } else if newPosition.contains(Position(x,y)) {
                    new_y.append(blockName)
                } else if cellSnapshot == blockName {
                    new_y.append(" ")
                } else {
                    new_y.append(cellSnapshot)
                }
            }
            newBoardSnapshot.append(new_y)
        }
        return try Board(snapshot: newBoardSnapshot, previous: board)
    }
    
}
