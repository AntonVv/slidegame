//
//  Errors.swift
//  SlideBoard
//
//  Created by Mac on 5/24/19.
//  Copyright © 2019 AV. All rights reserved.
//

import Foundation

enum BoardError: Error {
    case invalidJSONInput
    case isNotAvailable
}

enum GameError: Error {
    case invalidResult
}
