//
//  Position.swift
//  SlideBoard
//
//  Created by Mac on 5/15/19.
//  Copyright © 2019 AV. All rights reserved.
//

import Foundation

struct Position {
    
    let x: Int
    let y: Int
    
    init(_ x: Int,_ y: Int) {
        self.x = x
        self.y = y
    }
}

extension Position: Hashable {
    static func == (lhs: Position, rhs: Position) -> Bool {
        return lhs.x == rhs.x && lhs.y == rhs.y
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(x)
        hasher.combine(y)
    }
}

extension Position {
    static var zero: Position {
        return Position(0, 0)
    }
}
