//
//  Size.swift
//  SlideBoard
//
//  Created by Mac on 5/15/19.
//  Copyright © 2019 AV. All rights reserved.
//

import Foundation

struct Size {
    
    let width: Int
    let height: Int
    
    init(_ width: Int,_ height: Int) {
        self.width = width
        self.height = height
    }
}

extension Size: Hashable {
    static func == (lhs: Size, rhs: Size) -> Bool {
        return lhs.width == rhs.width && lhs.height == rhs.height
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(width)
        hasher.combine(height)
    }
}
