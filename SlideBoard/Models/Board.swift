//
//  Desk.swift
//  SlideBoard
//
//  Created by Mac on 5/12/19.
//  Copyright © 2019 AV. All rights reserved.
//

import Foundation

final class Board {
    
    var previous: Board?
    var move: Move?
    var moveNumber: Int
    
    let size: Size

    private(set) var blocks = [String:Block]()
    var blocksSnapshot: [Position:Block] = [:]
    var stateSnapshot = [[String]]()

    
    init(snapshot:[[String]], previous: Board? = nil, move: Move? = nil, moveNumber: Int = 0) throws {
        
        guard snapshot.isEmpty == false else {
            throw BoardError.invalidJSONInput
        }
        
        self.moveNumber = moveNumber
        self.previous = previous

        if let parentBoard = self.previous {
            self.moveNumber = parentBoard.moveNumber + 1
        }
        
        self.move = move
        stateSnapshot = snapshot
        self.size = Size(snapshot[0].count, snapshot.count)
        
        for (lineIndex, line) in snapshot.enumerated() {

            for (charIndex, char) in line.enumerated() {
                
                switch char {
                    
                case BlockIdentifierGenerator.borderIdentifier:
                    blocksSnapshot[Position(charIndex,lineIndex)] = Block.borderBlock
                    
                case BlockIdentifierGenerator.gateIdentifier:
                    blocksSnapshot[Position(charIndex,lineIndex)] = Block.gateBlock
                    
                case " ":
                    ()
                    
                default:
                    let blockPosition = Position(charIndex, lineIndex)
                    let blockSize = type(of: self).sizeOfBlock(withId: char, in: snapshot, xPosition: charIndex, yPosition: lineIndex)
                    let block = Block(size: blockSize, at: blockPosition, id: char)
                    
                    if blocks[char] == nil {
                        blocks[char] = block
                        
                        block.absolutePoints.forEach{ [weak self] in
                            guard let `self` = self else {
                                return
                            }
                            self.blocksSnapshot[$0] = block
                        }
                    }
                }
            }
        }
    }
    
    
    class func sizeOfBlock(withId: String, in snapshot:[[String]], xPosition x: Int, yPosition y: Int) -> Size {
        
        var width = 0
        var height = 0
        snapshot[y].lazy[x..<snapshot[y].endIndex].forEach { (char, charIndex, stop) in
            if char != withId {
                stop = true
            } else {
                width += 1
            }
        }
        
        snapshot.lazy[y..<snapshot.endIndex].forEach { (line, lineIndex, stop) in
            if line[x] != withId {
                stop = true
            } else {
                height += 1
            }
        }
        
        return Size(width, height)
    }
    
    func isMainBlockExited() -> Bool {
        
        var isMainBlockExited = true
        stateSnapshot.forEach { (line, lineIndex, lineStop) in
            
            line.forEach{ (character, charIndex, charStop) in
                
                if character == "b" {
                    isMainBlockExited = false
                    charStop = true
                }
            }
            if isMainBlockExited == false {
                lineStop = true
            }
        }
        return isMainBlockExited
    }

    func getBlock(at position: Position) -> Block? {
        return blocksSnapshot[position]
    }
    func getBlockName(at position: Position) -> String {
        if position.y < stateSnapshot.count && position.x < stateSnapshot[position.y].count {
            return stateSnapshot[position.y][position.x]
        }
        return " "
    }
    
    func getBlock(atX x: Int, y: Int) -> Block? {
        return getBlock(at: Position(x, y))
    }
}

extension Board {
    
    func getAllAvailableMoves() -> [Move] {
        
        let moveDirections: Set<MoveDirection> = [.top, .left, .bottom, .right]
        
        let previousMove = self.previous?.move
        
        var availableMoves = [Move]()
        
        for block in self.blocks.values {
            
            for direction in moveDirections {
                var possibleMove = Move(moveBlock: block, to: direction)
                
                if let previous = previousMove {
                    if possibleMove == previous.invertedMove {
                        continue
                    }
                }
                
                if self.isMoveAvailable(possibleMove, on: self) {
                    availableMoves.append(possibleMove)
                    
                    possibleMove.numOfSteps = 2
                    
                    if self.isMoveAvailable(possibleMove, on: self) {
                        availableMoves.append(possibleMove)
                    }
                }
            }
        }
        
        return availableMoves
    }
    
    func isMoveAvailable(_ move: Move, on board: Board) -> Bool {
        
        var isMoveAvailable = true
        move.getPredictedPositions().forEach {
            [weak self] in
            guard let `self` = self else {
                return
            }
            
            let newPosition = $0
            
            let currentBlockNameOnPosition = board.getBlockName(at: newPosition)
            
            if !self.isFreeCell(withName: currentBlockNameOnPosition, newName: move.block.id) && !self.mainBlockCanExitFromCell(withName: currentBlockNameOnPosition, newName: move.block.id) {
                isMoveAvailable = false
            }
        }
        return isMoveAvailable
    }
    
    func isFreeCell(withName: String, newName: String) -> Bool {
        return withName == " " || withName == newName
    }
    
    func mainBlockCanExitFromCell(withName: String, newName: String) -> Bool {
        return withName == "Z" && newName == "b"
    }
    
    var winPositionForMainBlock: Position {
        //TO-DO change to calculation
        return Position(2, 4)
    }
    
    func boardChain() -> [Board] {
        
        var board: Board? = self
        var boards = [Board]()
        while let nextBoard = board?.previous {
            boards.append(nextBoard)
            board = nextBoard
        }
        
        boards.reverse()
        return boards
    }
}

extension Board: CustomDebugStringConvertible {
    var debugDescription: String {
        return self.getCurrentSnapshot()
    }

    
    private func getCurrentSnapshot() -> String {
        
        var boardSnapshot = ""
        
        for y in 0..<self.size.height {
            
            for x in 0 ..< self.size.width {
                
                boardSnapshot.append(getBlockName(at: Position(x, y)))
            }
            boardSnapshot.append("\n")
        }
        boardSnapshot.append("\n")
        
        return boardSnapshot
    }
}

extension Board: Hashable {
    
    static func == (lhs: Board, rhs: Board) -> Bool {
        return lhs.stateSnapshot == rhs.stateSnapshot
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(stateSnapshot)
    }
}

