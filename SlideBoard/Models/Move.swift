//
//  Move.swift
//  SlideBoard
//
//  Created by Mac on 5/30/19.
//  Copyright © 2019 AV. All rights reserved.
//

import Foundation

enum MoveDirection {
    case top, bottom, left, right
    
    // return changes after move in direction
    func getChanges(in numberOfSteps: Int) -> (Int, Int) {
        switch self {
        case .top:
            return (0, -numberOfSteps)
        case .bottom:
            return (0, numberOfSteps)
        case .left:
            return (-numberOfSteps, 0)
        case .right:
            return (numberOfSteps, 0)
        }
    }
    
    mutating func invertDirection() {
        switch self {
        case .top:
            self = .bottom
        case .bottom:
            self = .top
        case .left:
            self = .right
        case .right:
            self = .left
        }
    }
}

struct Move {
    
    let block: Block
    var direction: MoveDirection
    var numOfSteps: Int = 1
    var priority: Int = 0
    
    init(moveBlock aBlock: Block, to aDirection: MoveDirection, in aNumOfSteps: Int = 1) {
        self.block = aBlock
        self.direction = aDirection
        self.numOfSteps = aNumOfSteps
    }

    
    // occupied positions by block after applying move
    func getPredictedPositions() -> [Position] { //return extreme point for direction
        let positionUpdate = direction.getChanges(in: numOfSteps)
        let currentPosition = block.position
        
        var occupiedPositions = [Position]()
        
        for w in 0..<block.size.width {
            
            for h in 0..<block.size.height {
                
                let newPosition = Position(currentPosition.x + w + positionUpdate.0, currentPosition.y + h + positionUpdate.1)
                
                occupiedPositions.append(newPosition)
            }
        }
        
        return occupiedPositions
    }
    
    var invertedMove: Move {
        var copy = self
        copy.direction.invertDirection()
        return copy
    }
}

extension Move: Equatable {
    static func == (lhs: Move, rhs: Move) -> Bool {
        return lhs.block.id == rhs.block.id && lhs.direction == rhs.direction
    }
}


